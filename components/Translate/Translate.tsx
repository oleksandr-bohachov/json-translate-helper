import React, { useEffect, useMemo, useState, useCallback, ChangeEvent } from "react";
import { Controller, useForm } from "react-hook-form";
import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import Tooltip from "@mui/material/Tooltip";

import { styled } from "@mui/material/styles";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import CloudDownloadIcon from "@mui/icons-material/CloudDownload";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";

const Input = styled("input")({
    display: "none",
});

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

import styles from "./Translate.module.scss";

const FORM_DOT_REPLACER = "___";
const keyToFieldName = (s: string): string => s.replace(/\./g, FORM_DOT_REPLACER);
const fieldNameToKey = (s: string): string => s.replaceAll(FORM_DOT_REPLACER, ".");

const readJsonFile = (file: File, onSuccess: (result: Record<string, string>[]) => void) => {
    const reader = new FileReader();

    reader.onload = function (evt: ProgressEvent<FileReader>) {
        if (!evt?.target?.result || typeof evt?.target?.result !== "string") {
            return;
        }

        try {
            const srcFileParsed = JSON.parse(evt.target.result);

            if (srcFileParsed) {
                const srcTKeys = Object.keys(srcFileParsed);
                const parsedToSrc = srcTKeys.reduce(
                    (all: Record<string, string>[], key: string) => {
                        return [...all, { key, value: srcFileParsed[key] }];
                    },
                    []
                );

                onSuccess(parsedToSrc);
            }
        } catch (e) {
            alert("File parse error");
        }
    };

    reader.readAsText(file);
};

const Translate = () => {
    const [src, setSrc] = useState<Record<string, string>[]>([]);
    const [target, setTarget] = useState<Record<string, string>[]>([]);
    const [namespace, setNamespace] = useState("");
    const [error, setError] = useState("");
    const [copiedToClipboard, setCopiedToClipboard] = useState(false);
    const [hideTranslated, setHideTranslated] = useState(false);
    const targetByID: Record<string, string> = useMemo(
        () =>
            target.reduce((all, { key, value }) => {
                return { ...all, [key]: value };
            }, {}),
        [target]
    );
    const getTranslatedValues = useCallback(() => {
        return src.reduce((all, { key }) => {
            return {
                ...all,
                [keyToFieldName(key)]: targetByID[key] || "",
            };
        }, {});
    }, [src, targetByID]);
    const {
        reset,
        control,
        handleSubmit,
        getValues,
        formState: { errors },
    } = useForm({
        mode: "onBlur",
        defaultValues: getTranslatedValues(),
    });
    const translations: Record<string, string> = getValues();
    const totalKeys = src.length;
    const totalTranslatedKeys = Object.keys(translations).filter(k => translations[k]).length;
    const hasMissingKeys = totalKeys > 0 && totalKeys - totalTranslatedKeys > 0;
    const missingKeysLabel = `${totalKeys - totalTranslatedKeys} of ${totalKeys} keys missing`;
    const hiddenByFilters =
        hideTranslated && target.length > 0
            ? Object.keys(targetByID).filter(k => targetByID[k]).length
            : 0;

    const handleClipboardCopy = (value: string) => {
        navigator.clipboard.writeText(value);

        setCopiedToClipboard(true);
    };

    const onSubmit = async () => {
        const translatedObj = Object.keys(translations).reduce((all, key) => {
            return {
                ...all,
                [fieldNameToKey(key)]: translations[key],
            };
        }, {});
        const dataStr =
            "data:text/json;charset=utf-8," +
            encodeURIComponent(JSON.stringify(translatedObj, undefined, 4));
        const dlAnchorElem = document.getElementById("downloadAnchorElem");

        if (dlAnchorElem) {
            dlAnchorElem.setAttribute("href", dataStr);
            dlAnchorElem.setAttribute("download", namespace);
            dlAnchorElem.click();
        }
    };

    const onSrcSelect = (event: ChangeEvent<HTMLInputElement>) => {
        if (!event || !event?.target?.files) {
            return;
        }

        const file = event?.target?.files[0];

        if (!file) {
            return;
        }

        readJsonFile(file, res => {
            setNamespace(file.name);
            setSrc(res);
        });
    };

    const onTargetSelect = (event: ChangeEvent<HTMLInputElement>) => {
        if (!event || !event?.target?.files) {
            return;
        }

        const file = event.target?.files[0];

        if (!file) {
            return;
        }

        if (file.name !== namespace) {
            setError(`Namespace mismatch! Select file named ${namespace} to proceed.`);

            return;
        }

        readJsonFile(file, res => {
            setTarget(res);
        });
    };

    // update form with translations when user selects source or target
    useEffect(() => {
        reset(getTranslatedValues());
    }, [reset, src, target, getTranslatedValues]);

    return (
        <>
            <h2>JSON Translate Helper v0.0.1</h2>
            <form onSubmit={handleSubmit(onSubmit)} noValidate style={{ width: "100%" }}>
                <Snackbar
                    open={Boolean(error)}
                    autoHideDuration={6000}
                    onClose={() => setError("")}>
                    <Alert onClose={() => setError("")} severity="warning" sx={{ width: "100%" }}>
                        {error}
                    </Alert>
                </Snackbar>

                <Snackbar
                    open={Boolean(copiedToClipboard)}
                    autoHideDuration={3000}
                    onClose={() => setCopiedToClipboard(false)}>
                    <Alert
                        onClose={() => setCopiedToClipboard(false)}
                        severity="success"
                        sx={{ width: "100%" }}>
                        Copied to clipboard
                    </Alert>
                </Snackbar>

                <div>
                    Namespace: <strong>{namespace || "Not selected"}</strong>
                    {totalKeys > 0 && (
                        <Chip
                            label={
                                hasMissingKeys
                                    ? missingKeysLabel
                                    : `All done, ${src.length - target.length} strings added`
                            }
                            color={hasMissingKeys ? "error" : "success"}
                            variant="outlined"
                            size="small"
                            style={{ marginLeft: "0.5rem" }}
                        />
                    )}
                    {hiddenByFilters > 0 ? (
                        <Chip
                            label={`${hiddenByFilters} strings hidden by filters`}
                            color="info"
                            variant="outlined"
                            size="small"
                            style={{ marginLeft: "0.5rem" }}
                        />
                    ) : null}
                </div>

                <br />

                <div className={styles.toolbar}>
                    <Stack direction="row" spacing={1}>
                        <label htmlFor="src-file">
                            <Input
                                accept="application/json"
                                id="src-file"
                                type="file"
                                onChange={onSrcSelect}
                            />
                            <Button
                                variant="contained"
                                size="large"
                                component="span"
                                startIcon={<CloudUploadIcon />}
                                color="info">
                                Upload Source JSON
                            </Button>
                        </label>
                        <label htmlFor="target-file">
                            <Input
                                accept="application/json"
                                id="target-file"
                                type="file"
                                onChange={onTargetSelect}
                            />
                            <Button
                                variant="contained"
                                size="large"
                                component="span"
                                startIcon={<CloudUploadIcon />}
                                color="info"
                                disabled={!namespace}>
                                Upload Target JSON
                            </Button>
                        </label>
                        {src.length && target.length ? (
                            <Tooltip title="Show/Hide translated strings">
                                <IconButton
                                    color="primary"
                                    onClick={() => setHideTranslated(!hideTranslated)}>
                                    {hideTranslated ? <VisibilityOffIcon /> : <VisibilityIcon />}
                                </IconButton>
                            </Tooltip>
                        ) : null}
                        <Button
                            type="submit"
                            variant="contained"
                            size="large"
                            startIcon={<CloudDownloadIcon />}
                            color="success"
                            disabled={hasMissingKeys || !totalKeys}>
                            Download Translated JSON
                        </Button>
                        <a id="downloadAnchorElem" style={{ display: "none" }} />
                    </Stack>
                </div>

                <br />

                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Translation Key</TableCell>
                                <TableCell>English</TableCell>
                                <TableCell>Translation</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {src.map(({ key, value }) => {
                                const fieldName = keyToFieldName(key);
                                const needToHide = hideTranslated && Boolean(targetByID[key]);

                                return (
                                    <TableRow
                                        className={needToHide ? styles.hidden : ""}
                                        key={key}
                                        sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                                        <TableCell
                                            component="th"
                                            scope="row"
                                            style={{ color: "#888" }}>
                                            {key}
                                        </TableCell>
                                        <TableCell
                                            component="th"
                                            scope="row"
                                            style={{ cursor: "copy" }}
                                            onClick={() => handleClipboardCopy(value)}>
                                            <Tooltip title="Click to copy">
                                                <span>{value}</span>
                                            </Tooltip>
                                        </TableCell>
                                        <TableCell align="right" width="40%">
                                            <Controller
                                                name={fieldName}
                                                control={control}
                                                rules={{
                                                    required: "Translation is mandatory",
                                                }}
                                                render={({ field }) => {
                                                    return (
                                                        <TextField
                                                            {...field}
                                                            id="outlined-required"
                                                            label="Translation"
                                                            size="small"
                                                            margin="dense"
                                                            variant="outlined"
                                                            InputLabelProps={{ shrink: true }}
                                                            error={Boolean(errors[fieldName])}
                                                            helperText={
                                                                errors[fieldName] &&
                                                                errors[fieldName].message
                                                            }
                                                            multiline
                                                            fullWidth
                                                            required
                                                        />
                                                    );
                                                }}
                                            />
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </form>
        </>
    );
};

export default Translate;
