import type { NextPage } from "next";

import Head from "next/head";
import React from "react";

import Translate from "../components/Translate/Translate";

import styles from "../styles/Home.module.scss";

const Home: NextPage = () => {
    return (
        <div className={styles.container}>
            <Head>
                <title>JSON Translation Helper</title>
                <link rel="shortcut icon" href="/favicon.ico" />
                <link
                    rel="stylesheet"
                    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
                />
            </Head>
            <main className={styles.main}>
                <Translate />
            </main>
        </div>
    );
};

export default Home;
